import React, { Component } from "react";
import "./App.css";

interface Props {}
interface State {
  paused: boolean;
  timeElapsed: number;
}

export default class App extends Component<Props, State> {
  private timerId!: NodeJS.Timer;

  constructor(props: Props) {
    super(props);
    this.state = { paused: true, timeElapsed: 0 };

    this.tick = this.tick.bind(this);
  }

  tick() {
    this.setState((state) => ({ timeElapsed: state.timeElapsed + 10 }));
  }

  onStartHandler() {
    if (this.state.paused) {
      this.timerId = setInterval(this.tick, 10);
      this.setState((state) => ({ ...state, paused: false }));
    } else {
      clearInterval(this.timerId);
      this.setState((state) => ({ ...state, paused: true }));
    }
  }

  onResetHandler() {
    this.setState({ paused: true, timeElapsed: 0 });
    clearInterval(this.timerId);
  }

  formatTime(timeElapsed: number): string {
    timeElapsed = timeElapsed / 1000;
    const hours = Math.floor(timeElapsed / 3600);
    const minutes = Math.floor(timeElapsed / 60) % 60;
    const seconds = Math.floor(timeElapsed) % 60;

    return `${hours.toString().padStart(2, "0")} : ${minutes
      .toString()
      .padStart(2, "0")} : ${seconds.toString().padStart(2, "0")}`;
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
  }

  render() {
    return (
      <div className="app">
        <div className="stopwatch">
          <h1 className="stopwatch__header">React Stopwatch 🕑</h1>
          <div className="stopwatch__display">
            <div className="stopwatch__displayTimer">
              <p className="stopwatch__displayTime">
                {this.formatTime(this.state.timeElapsed)}
                <span className="stopwatch__displayTime--millis">
                  {(this.state.timeElapsed % 1000).toString().padStart(3, "0")}
                </span>
              </p>
            </div>
            <div className="stopwatch__displayButtons">
              <button
                className="stopwatch__displayButton"
                onClick={this.onStartHandler.bind(this)}>
                {this.state.paused ? "Start" : "Pause"}
              </button>
              <button
                className="stopwatch__displayButton"
                onClick={this.onResetHandler.bind(this)}>
                Reset
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
